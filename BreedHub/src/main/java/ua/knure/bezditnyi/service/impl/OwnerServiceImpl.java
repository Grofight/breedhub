package ua.knure.bezditnyi.service.impl;

import org.hibernate.SessionFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.knure.bezditnyi.dao.interfaces.OwnerDao;
import ua.knure.bezditnyi.dao.interfaces.PetDao;
import ua.knure.bezditnyi.model.Owner;
import ua.knure.bezditnyi.model.Pet;
import ua.knure.bezditnyi.service.interfaces.OwnerService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OwnerServiceImpl implements OwnerService {

    @Resource
    private SessionFactory factory;
    @Resource
    private OwnerDao ownerDao;
    @Resource
    private PetDao petDao;

    @Transactional
    public Integer registerOwner(Owner owner) {
        return ownerDao.addOwner(owner);
    }

    @Transactional
    public void editOwner(Owner owner) {
        owner.setPets(owner.getPets());
        ownerDao.editOwner(owner);
    }

    @Transactional
    public Owner getOwnerById(Integer ownerId) {
        return ownerDao.getOwnerById(ownerId);
    }

    @Override
    public boolean isOwnerExist(String email) {
        return ownerDao.isOwnerExist(email);
    }

    @Transactional
    public List<Owner> getOwners() {
        return ownerDao.getOwners();
    }

    @Transactional
    public void addPet(Integer ownerId, Pet pet) {
        Owner owner = ownerDao.getOwnerById(ownerId);
        owner.getPets().add(pet);
        pet.setOwner(owner);
        ownerDao.editOwner(owner);
    }

    @Transactional
    public void removePet(Integer petId) {
        petDao.removePet(petId);
    }

    @Transactional
    public void editPet(Pet pet) {
        petDao.editPet(pet);
    }

    @Override
    public String getCurrentOwnerEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    @Override
    public Owner getOwnerByEmail(String email) {
        return ownerDao.getOwnerByEmail(email);
    }

    @Override
    public Pet getPetById(int id) {
        return petDao.getPetById(id);
    }

    @Override
    public List<Pet> getCurrentOwnerPets() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Owner currentOwner = getOwnerByEmail(authentication.getName());
        return ownerDao.getPets(currentOwner.getId());
    }
}
