package ua.knure.bezditnyi.service.interfaces;

import ua.knure.bezditnyi.model.Owner;
import ua.knure.bezditnyi.model.Pet;

import java.util.List;

public interface OwnerService {

    Integer registerOwner(Owner owner);

    void editOwner(Owner owner);

    Owner getOwnerById(Integer ownerId);

    Owner getOwnerByEmail(String email);

    boolean isOwnerExist(String email);

    List<Owner> getOwners();

    Pet getPetById(int id);

    List<Pet> getCurrentOwnerPets();

    void addPet(Integer ownerId, Pet pet);

    void removePet(Integer petId);

    void editPet(Pet pet);

    String getCurrentOwnerEmail();
}
