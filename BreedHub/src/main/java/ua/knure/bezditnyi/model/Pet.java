package ua.knure.bezditnyi.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "thoroughbred")
    private boolean thoroughbred;

    @Column(name = "breed")
    private String breed;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "description")
    private String description;

    @ManyToOne()
    @Cascade(value = {CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "owner_id", updatable = false)
    private Owner owner;

    public Pet() {

    }

    public Pet(String name, boolean thoroughbred, String breed, Date birthday, String description) {
        this.name = name;
        this.thoroughbred = thoroughbred;
        this.breed = breed;
        this.birthday = birthday;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isThoroughbred() {
        return thoroughbred;
    }

    public void setThoroughbred(boolean thoroughbred) {
        this.thoroughbred = thoroughbred;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
