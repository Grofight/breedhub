package ua.knure.bezditnyi.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ua.knure.bezditnyi.model.Owner;
import ua.knure.bezditnyi.model.Pet;
import ua.knure.bezditnyi.service.interfaces.OwnerService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/owner",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Resource
    private OwnerService ownerService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Owner> getOwner() {
        String email = ownerService.getCurrentOwnerEmail();
        Owner owner = ownerService.getOwnerByEmail(email);
        return getOwnerById(owner.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Owner> getOwnerById(@PathVariable("id") int id) {
        Owner owner = ownerService.getOwnerById(id);
        if (owner == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(owner, HttpStatus.OK);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public ResponseEntity<Owner> editOwner(@RequestBody Owner owner) {
        String email = ownerService.getCurrentOwnerEmail();
        Owner currentOwner = ownerService.getOwnerByEmail(email);
        if (currentOwner == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ownerService.editOwner(owner);
        return new ResponseEntity<>(currentOwner, HttpStatus.OK);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Void> register(@RequestBody Owner owner, UriComponentsBuilder ucBuilder) {
        if (ownerService.isOwnerExist(owner.getEmail())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        ownerService.registerOwner(owner);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/owner").buildAndExpand(owner.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/addPet", method = RequestMethod.POST)
    public ResponseEntity<Void> addPet(@RequestBody Pet pet, UriComponentsBuilder ucBuilder) {
        String email = ownerService.getCurrentOwnerEmail();
        Owner owner = ownerService.getOwnerByEmail(email);
        if (owner == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ownerService.addPet(owner.getId(), pet);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/owner/pet/{id}").buildAndExpand(owner.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/pet/{id}", method = RequestMethod.GET)
    public ResponseEntity<Pet> getPetById(@PathVariable("id") Integer petId) {
        return new ResponseEntity<Pet>(ownerService.getPetById(petId), HttpStatus.OK);
    }

    @RequestMapping(value = "/pets", method = RequestMethod.GET)
    public ResponseEntity<List<Pet>> getPets() {
        return new ResponseEntity<List<Pet>>(ownerService.getCurrentOwnerPets(), HttpStatus.OK);
    }

    @RequestMapping(value = "/removePet/{id}", method = RequestMethod.DELETE)
    public void removePet(@PathVariable("id") Integer petId) {
        ownerService.removePet(petId);
    }

    @RequestMapping(value = "/editPet", method = RequestMethod.PUT)
    public void editPet(@RequestBody Pet pet) {
        ownerService.editPet(pet);
    }
}
