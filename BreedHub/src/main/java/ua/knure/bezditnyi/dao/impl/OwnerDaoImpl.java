package ua.knure.bezditnyi.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.knure.bezditnyi.dao.exception.DaoException;
import ua.knure.bezditnyi.dao.interfaces.OwnerDao;
import ua.knure.bezditnyi.model.Owner;
import ua.knure.bezditnyi.model.Pet;

import javax.annotation.Resource;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class OwnerDaoImpl implements OwnerDao {

    private final String DUPLICATE_USER_EXCEPTION_MESSAGE = "There are several users with the specified email: ";

    private final String GET_OWNER_BY_EMAIL_QUERY = "FROM Owner WHERE email = :email";
    private final String GET_PETS_OF_OWNER_QUERY = "FROM Pet WHERE owner_id = :ownerId";
//    private final String GET_PETS_OF_OWNER_QUERY = "FROM Pet WHERE owner_id = Owner.id WHERE email = :email";

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public Integer addOwner(Owner owner) {
        return (Integer) session().save(owner);
    }

    @Override
    public void editOwner(Owner owner) {
        session().update(owner);
    }

    @Override
    public Owner getOwnerById(int ownerId) {
        return session().get(Owner.class, ownerId);
    }

    @Override
    public Owner getOwnerByEmail(String email) {
        Query query = session().createQuery(GET_OWNER_BY_EMAIL_QUERY);
        query.setParameter("email", email);
        
        return getOwnerFromResultList(query.getResultList());
    }

    @Override
    public boolean isOwnerExist(String email) {
        Query query = session().createQuery(GET_OWNER_BY_EMAIL_QUERY);
        query.setParameter("email", email);

        return getOwnerFromResultList(query.getResultList()) != null;
    }

    @Override
    public List<Pet> getPets(int ownerId) {
        Query query = session().createQuery(GET_PETS_OF_OWNER_QUERY);
        query.setParameter("ownerId", ownerId);

        return (List<Pet>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Owner> getOwners() {
        return (List<Owner>) session()
                .createCriteria(Owner.class)
                .list();
    }
    
    private Owner getOwnerFromResultList(List<Owner> owners) {
        if (owners.size() == 1)
            return owners.get(0);
        else if (owners.size() == 0)
            return null;
        else
            throw new DaoException(DUPLICATE_USER_EXCEPTION_MESSAGE + owners.get(0).getEmail());
    }
    
    private Session session() {
        return sessionFactory.getCurrentSession();
    }
}
