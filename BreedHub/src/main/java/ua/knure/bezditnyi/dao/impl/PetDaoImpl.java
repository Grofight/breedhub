package ua.knure.bezditnyi.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.knure.bezditnyi.dao.interfaces.PetDao;
import ua.knure.bezditnyi.model.Pet;

import javax.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class PetDaoImpl implements PetDao {

    @Resource
    private SessionFactory sessionFactory;

    public Integer addPet(Pet pet) {
        return (Integer) session().save(pet);
    }

    public void editPet(Pet pet) {
        session().update(pet);
    }

    public void removePet(Integer petId) {
        Pet pet = getPetById(petId);
        session().delete(pet);
    }

    public Pet getPetById(int petId) {
        return session().load(Pet.class, petId);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public List<Pet> getPets() {
        return session()
                .createCriteria(Pet.class)
                .list();
    }

    public Session session() {
        return sessionFactory.getCurrentSession();
    }
}
