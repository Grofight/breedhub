package ua.knure.bezditnyi.dao.exception;

public class DaoException extends RuntimeException {

    public DaoException(String message) {
        super(message);
    }
}
