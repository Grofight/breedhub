package ua.knure.bezditnyi.dao.interfaces;

import ua.knure.bezditnyi.model.Pet;

import java.util.List;

public interface PetDao {

    Integer addPet(Pet pet);

    void editPet(Pet pet);

    void removePet(Integer petId);

    Pet getPetById(int petId);

    List<Pet> getPets();
}
