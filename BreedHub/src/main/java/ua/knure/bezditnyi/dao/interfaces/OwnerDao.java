package ua.knure.bezditnyi.dao.interfaces;

import ua.knure.bezditnyi.model.Owner;
import ua.knure.bezditnyi.model.Pet;

import java.util.List;

public interface OwnerDao {

    Integer addOwner(Owner owner);

    void editOwner(Owner owner);

    Owner getOwnerById(int ownerId);

    Owner getOwnerByEmail(String email);

    boolean isOwnerExist(String email);

    List<Pet> getPets(int ownerId);

    List<Owner> getOwners();
}
